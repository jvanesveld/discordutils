import * as loaders from "./load.ts";
import { empty, getYoutubeSources } from "../../deps.ts";
import { spotifyMatchers } from "./matchers.ts";
import { findDefined } from "../utils/mod.ts";

type Loaders = keyof typeof loaders;

type Artist = {
  name: string;
};

type Track = {
  name: string;
  artists?: Artist[];
};

type SpotifyRequest = [type: Loaders, id: string];

function mapTracksToQuery(tracks: Track[]) {
  const queries = tracks.map(
    (track) => `${track.name} ${track.artists?.[0]?.name || ""}`,
  );
  return queries;
}

export async function loadSpotify(request: SpotifyRequest) {
  try {
    const result = await loaders[request[0]](request[1]);
    const songs = mapTracksToQuery(result);
    return getYoutubeSources(...songs);
  } catch (error) {
    console.log(error);
    return empty();
  }
}

export function matchSpotify(test: string): SpotifyRequest | undefined {
  const matchers = Object.entries(spotifyMatchers);
  const find = ([key, regex]: [string, RegExp]) => {
    const match = test.match(regex);
    const result = [key, match?.[1]];
    return match !== null ? (result as SpotifyRequest) : undefined;
  };
  return findDefined(matchers, find);
}
