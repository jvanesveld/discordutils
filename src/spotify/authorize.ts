let keys:
  | {
      id: string;
      secret: string;
    }
  | undefined;

type TokenData = {
  access_token: string;
  token_type: string;
  expires_in: number;
};

function getStoredSpotify() {
  const spotifyJSON = localStorage.getItem("spotify");
  if (spotifyJSON) {
    return JSON.parse(spotifyJSON);
  }
  return {
    token: "",
    expires: "",
  };
}

export function setSpotifyKeys(id: string, secret: string) {
  keys = {
    id: id,
    secret: secret,
  };
}

export async function getToken() {
  await refreshToken();
  return getStoredSpotify().token;
}

/**
 * Gets new token if old one is no longer valid
 */
async function refreshToken() {
  if (isExpired()) {
    await fetchToken();
  }
}
/**
 * Checks if current spotify token is still valid
 * @returns true if token is no longer valid
 */
function isExpired() {
  const expires = getStoredSpotify().expires;
  if (expires === "") {
    return true;
  }
  const expireDate = new Date(expires);
  return expireDate.getTime() <= new Date().getTime();
}

/**
 * Fetch new spotify token and save it in memory
 */
async function fetchToken() {
  if (keys === undefined) {
    throw "Spotify keys not set!";
  }
  const body = encodeXWWWPostBody({ grant_type: "client_credentials" });
  const response = await fetch("https://accounts.spotify.com/api/token", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      Authorization: `Basic ${btoa(`${keys.id}:${keys.secret}`)}`,
    },
    body,
  });
  const data: TokenData = await response.json();
  const expireDate = new Date(new Date().getTime() + 1000 * data.expires_in);
  localStorage.setItem(
    "spotify",
    JSON.stringify({
      token: data.access_token,
      expires: expireDate.toJSON(),
    })
  );
}

function encodeXWWWPostBody(details: { [key: string]: string }): string {
  const formBody = [];
  for (const property in details) {
    const encodedKey = encodeURIComponent(property);
    const encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  return formBody.join("&");
}
