import { group, regex, zeroOrMore, notCharset } from "../../deps.ts";

const idMatch = group`${zeroOrMore`${notCharset`?`}`}`;

const artist = regex`https://open.spotify.com/artist/${idMatch}`;
const album = regex`https://open.spotify.com/album/${idMatch}`;
const playlist = regex`https://open.spotify.com/playlist/${idMatch}`;

export const spotifyMatchers = { artist, album, playlist };
