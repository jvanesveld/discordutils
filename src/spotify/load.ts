import { generateParams } from "../utils/params.ts";
import { getToken } from "./authorize.ts";

type TrackList = {
  items: any[];
  next?: string;
};

type TopSongs = {
  tracks: any[];
};

async function fetchSpotify<T = any>(url: string): Promise<T> {
  const response = await fetch(url, {
    headers: {
      Authorization: `Bearer ${await getToken()}`,
    },
  });
  return await response.json();
}

async function collect(url: string) {
  let response = await fetchSpotify<TrackList>(url);
  let tracks = response.items;
  while (response.next) {
    response = await fetchSpotify<TrackList>(response.next);
    tracks = tracks.concat(response.items);
  }
  return tracks;
}

export async function playlist(id: string) {
  return (await collect(`https://api.spotify.com/v1/playlists/${id}/tracks`))
    .map((value) => value.track)
    .filter((value) => value !== null && value !== undefined);
}

export function album(id: string) {
  return collect(`https://api.spotify.com/v1/albums/${id}/tracks`);
}

export async function artist(id: string) {
  const params = generateParams({
    market: "NL",
  });
  const response = await fetchSpotify<TopSongs>(
    `https://api.spotify.com/v1/artists/${id}/top-tracks?${params}`
  );
  return response.tracks;
}
