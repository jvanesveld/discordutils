import { generateParams } from "../utils/mod.ts";
import { getToken } from "./authorize.ts";

export enum SearchTypes {
  Album = "album",
  Track = "track",
  Artist = "artist",
  Playlist = "playlist",
  Show = "show",
  Episode = "episode",
}

export interface SearchOptions {
  market?: string;
  limit?: number;
  offset?: number;
}

/**
 * Take a random result from the top 1000
 * @param query Search query, same is you would type in on spotify (including for example genre:rock, year: 1980, etc..)
 * @param type The type of results you want (tracks, albums, etc..)
 * @returns Spotify API search results
 */
export async function searchRandom(query: string, type: SearchTypes[]) {
  return await search(query, type, {
    limit: 1,
    offset: Math.round(Math.random() * 1000),
  });
}

/**
 * Searches spotify for the given search query
 * @param query Search query, same is you would type in on spotify (including for example genre:rock, year: 1980, etc..)
 * @param type The type of results you want (tracks, albums, etc..)
 * @returns Spotify API search results
 */
export async function search(
  query: string,
  type: SearchTypes[],
  options?: SearchOptions
) {
  const params = generateParams({
    q: query,
    type: type.join(),
    ...options,
  });
  const response = await fetch(`https://api.spotify.com/v1/search?${params}`, {
    headers: {
      Authorization: `Bearer ${await getToken()}`,
    },
  });
  return await response.json();
}
