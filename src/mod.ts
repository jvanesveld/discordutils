import {
  AudioBot,
  Bot,
  CompleteDesiredProperties,
  DesiredPropertiesBehavior,
} from "../deps.ts";
import { createApp, getGuildApps } from "./application.ts";
import { audioQuery } from "./audio-query.ts";
import { ShortP } from "./events/args.ts";

export type BaseBot<TProps extends ShortP> = AudioBot<
  Bot<CompleteDesiredProperties<TProps>, DesiredPropertiesBehavior.RemoveKey>
>;

export type CoreBot<TProps extends ShortP> = BaseBot<TProps> & {
  getGuildApps: typeof getGuildApps;
  createApp: typeof createApp;
  loadSource: typeof audioQuery;
};

export function addCoreFunctions<
  TProps extends ShortP,
  J extends Record<string, any>,
>(
  bot: AudioBot<
    Bot<CompleteDesiredProperties<TProps>, DesiredPropertiesBehavior.RemoveKey>
  >,
  events: J,
) {
  return Object.assign(bot, {
    ...events,
    createApp,
    getGuildApps,
    loadSource: audioQuery,
  });
}
