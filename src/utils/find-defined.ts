export function findDefined<T, J>(values: Iterable<T>, find: (value: T) => J) {
  for (const value of values) {
    const found = find(value);
    if (found !== undefined) {
      return found;
    }
  }
}
