export type GetNext<T> = () => Promise<IteratorResult<Awaited<T>, void>>;

export function pushIter<T>(history = true) {
  const values: T[] = [];
  let resolvers: ((value: T) => void)[] = [];
  const pushValue = (value: T) => {
    if (resolvers.length <= 0 && history) {
      values.push(value);
    }
    for (const resolve of resolvers) {
      resolve(value);
    }
    resolvers = [];
  };
  const pullValue = () => {
    return new Promise<T>((resolve) => {
      if (values.length > 0 && history) {
        const value = values.shift();
        resolve(value as T);
      } else {
        resolvers.push(resolve);
      }
    });
  };
  return {
    push: pushValue,
    getIterator: () => iterFromPull(pullValue),
    getIterNext: (): GetNext<T> => {
      const iter = iterFromPull(pullValue);
      return () => {
        return iter.next();
      };
    },
  };
}

async function* iterFromPull<T>(pullValue: () => Promise<T>) {
  while (true) {
    yield await pullValue();
  }
}
