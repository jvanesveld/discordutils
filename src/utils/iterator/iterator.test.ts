import { assertEquals } from "https://deno.land/std@0.104.0/testing/asserts.ts";
import { EventSource, OneShotSource } from "../event-source.ts";
import { combineIter } from "./combine.ts";

Deno.test({
  name: "One Shot",
  fn: async () => {
    const source = new OneShotSource<number>();
    source.trigger(12);
    source.trigger(20);
    const streamA = source.stream();
    assertEquals(12, await streamA.nextValue());
    const streamB = source.stream();
    assertEquals(12, await streamB.nextValue());
  },
});

Deno.test({
  name: "Filter",
  fn: async () => {
    const source = new EventSource<number>();
    const from = source.stream();
    const to = from.filter((value) => {
      return value > 10;
    });
    source.trigger(1);
    source.trigger(20);
    source.trigger(4);
    source.trigger(21);
    source.trigger(2);
    assertEquals(20, await to.nextValue());
    assertEquals(21, await to.nextValue());
  },
});

Deno.test({
  name: "Map",
  fn: async () => {
    const source = new EventSource<number>();
    const from = source.stream();
    const to = from.map((value) => value + 1);
    source.trigger(1);
    source.trigger(2);
    source.trigger(13);
    assertEquals(2, await to.nextValue());
    assertEquals(3, await to.nextValue());
    assertEquals(14, await to.nextValue());
  },
});

Deno.test({
  name: "Filtered map",
  fn: async () => {
    const source = new EventSource<number>();
    const from = source.stream();
    const to = from.map((value) => value + 1).filter((value) => value >= 6);
    source.trigger(1);
    source.trigger(5);
    source.trigger(10);
    source.trigger(3);
    source.trigger(13);
    assertEquals(6, await to.nextValue());
    assertEquals(11, await to.nextValue());
    assertEquals(14, await to.nextValue());
  },
});

Deno.test({
  name: "Combine",
  fn: async () => {
    const sourceA = new EventSource<number>();
    const sourceB = new EventSource<string>();
    const stream = sourceA.stream().combine(sourceB.stream());
    sourceA.trigger(1);
    sourceB.trigger("Hello");
    sourceA.trigger(10);
    sourceB.trigger("World!");
    assertEquals(1, await stream.nextValue());
    assertEquals("Hello", await stream.nextValue());
    assertEquals(10, await stream.nextValue());
    assertEquals("World!", await stream.nextValue());
  },
});

Deno.test({
  name: "Combine multiple",
  fn: async () => {
    const sourceA = new EventSource<number>();
    const sourceB = new EventSource<string>();
    const sourceC = new EventSource<boolean>();
    const stream = sourceA.stream().combine(sourceB.stream(), sourceC.stream());
    sourceA.trigger(1);
    sourceB.trigger("Hello");
    sourceA.trigger(10);
    sourceB.trigger("World!");
    assertEquals(1, await stream.nextValue());
    assertEquals("Hello", await stream.nextValue());
    assertEquals(10, await stream.nextValue());
    assertEquals("World!", await stream.nextValue());
  },
});

Deno.test({
  name: "Combine all",
  fn: async () => {
    const sourceA = new EventSource<number>();
    const sourceB = new EventSource<string>();
    const sourceC = new EventSource<boolean>();
    const stream = combineIter(
      sourceA.stream(),
      sourceB.stream(),
      sourceC.stream()
    );
    sourceA.trigger(1);
    sourceB.trigger("Hello");
    sourceC.trigger(true);
    sourceA.trigger(10);
    sourceB.trigger("World!");
    assertEquals(1, await stream.nextValue());
    assertEquals("Hello", await stream.nextValue());
    assertEquals(true, await stream.nextValue());
    assertEquals(10, await stream.nextValue());
    assertEquals("World!", await stream.nextValue());
  },
});

Deno.test({
  name: "Combine filtered map",
  fn: async () => {
    const sourceA = new EventSource<number>();
    const sourceB = new EventSource<string>();
    const stream = sourceA
      .stream()
      .combine(sourceB.stream())
      .filter((value) => typeof value === "number");
    sourceA.trigger(1);
    sourceB.trigger("Hello");
    sourceA.trigger(10);
    sourceB.trigger("World!");
    assertEquals(1, await stream.nextValue());
    assertEquals(10, await stream.nextValue());
  },
});
