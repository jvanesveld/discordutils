export function generateParams(params: {
  [key: string]: string | number | boolean;
}): string {
  const array = Object.entries(params);
  return new URLSearchParams(
    array.map(([key, value]) => [key, value.toString()])
  ).toString();
}
