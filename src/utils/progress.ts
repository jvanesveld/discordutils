export function trackProgress<T, J = undefined>(
  prefix = "Loading: ",
  max_wait = 2000,
  onDone?: (loaded: [T, J][]) => void
) {
  const encoder = new TextEncoder();
  const enc = (s: string) => encoder.encode(s);
  const loaded: [T, J][] = [];
  const itemsPending: T[] = [];
  let lastLength = 0;
  let done: number;

  async function log() {
    const update = `${prefix}${loaded.length}/${
      itemsPending.length + loaded.length
    }`;
    await Deno.stdout.write(
      enc(update + " ".repeat(Math.max(0, lastLength - update.length)) + "\r")
    );
    lastLength = update.length;
    if (done !== undefined) {
      clearTimeout(done);
    }
    if (itemsPending.length === 0) {
      done = setTimeout(() => {
        onDone?.(loaded);
      }, max_wait);
    }
  }

  function pending(value: T) {
    itemsPending.push(value);
    log();
  }

  function confirm(value: T, data: J) {
    const loadedValue = itemsPending.splice(itemsPending.indexOf(value), 1)[0];
    loaded.push([loadedValue, data]);
    log();
  }

  return { pending, confirm, log };
}
