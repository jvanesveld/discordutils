import { pushIter, addIterUtils } from "./iterator/mod.ts";

export type Listener<T> = (arg: T) => void;

export class EventSource<T> {
  listeners: Listener<T>[] = [];

  trigger(arg: T) {
    for (const listener of this.listeners) {
      listener(arg);
    }
  }

  addListener = (listener: Listener<T>) => {
    this.listeners.push(listener);
    return () => {
      this.removeListener(listener);
    };
  };

  removeListener = (listener: Listener<T>) => {
    const index = this.listeners.indexOf(listener);
    this.listeners.splice(index, 1);
  };

  stream() {
    const { push, getIterator } = pushIter<T>();
    this.addListener(push);
    return addIterUtils<T>(getIterator());
  }
}

export class OneShotSource<T> extends EventSource<T> {
  value: T | undefined;

  override trigger(arg: T) {
    if (this.value) {
      return;
    } else {
      this.value = arg;
      for (const listener of this.listeners) {
        listener(arg);
      }
    }
  }

  addListener = (listener: Listener<T>) => {
    if (this.value) {
      listener(this.value);
      return () => undefined;
    } else {
      this.listeners.push(listener);
      return () => {
        this.removeListener(listener);
      };
    }
  };
}
