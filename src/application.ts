import { ApplicationCommand, CreateApplicationCommand } from "../deps.ts";
import { ShortP } from "./events/mod.ts";
import { CoreBot } from "./mod.ts";
import { asArray } from "./utils/array.ts";
import { trackProgress } from "./utils/progress.ts";

async function getAllGuildApps<TProps extends ShortP>(bot: CoreBot<TProps>) {
  const ids = (await bot.rest.getGuilds()).map((guild) => guild.id!);
  const all = [];
  for (const id of ids) {
    all.push(...(await bot.helpers.getGuildApplicationCommands(id)));
  }
  return all;
}

export async function getGuildApps<TProps extends ShortP>(
  this: CoreBot<TProps>,
  guildId: bigint,
) {
  return await this.helpers.getGuildApplicationCommands(guildId);
}

export async function createApp<TProps extends ShortP>(
  this: CoreBot<TProps>,
  config: CreateApplicationCommand,
  guilds: bigint | bigint[],
) {
  getTracker(this).pending(config);
  const cmdPromises: Promise<ApplicationCommand>[] = [];
  for (const guildId of asArray(guilds)) {
    const commands = await this.getGuildApps(guildId);
    const existing = commands.find((cmd) => cmd.name === config.name);
    if (existing !== undefined) {
      this.helpers
        .editGuildApplicationCommand(existing.id, guildId, config)
        .then((cmd) => {
          Object.assign(existing, cmd);
        });
      cmdPromises.push(Promise.resolve(existing));
    } else {
      cmdPromises.push(
        this.helpers.createGuildApplicationCommand(config, guildId),
      );
    }
  }
  const cmds = await Promise.all(cmdPromises);

  getTracker(this).confirm(config, cmds);
  return cmds;
}

let tracker:
  | undefined
  | ReturnType<
    typeof trackProgress<CreateApplicationCommand, ApplicationCommand[]>
  >;

function getTracker<TProps extends ShortP>(
  bot: CoreBot<TProps>,
) {
  if (tracker === undefined) {
    tracker = trackProgress("Loading: ", 2000, (loaded) => {
      const names = loaded.map(([config, _loaded]) => config.name).flat();
      console.log(`Loaded: ${names.join(", ")}`);
      removeDangling(bot, loaded.map(([_config, loaded]) => loaded).flat());
    });
  }
  return tracker;
}

export async function removeDangling<TProps extends ShortP>(
  bot: CoreBot<TProps>,
  loaded: ApplicationCommand[],
) {
  const removePromise = [];
  const cmds = await getAllGuildApps(bot);
  const tracker = trackProgress<ApplicationCommand>(
    "Removing: ",
    1000,
    (cmds) =>
      console.log(`Removed: ${cmds.map(([cmd]) => cmd.name).join(", ")}`),
  );
  for (const cmd of cmds) {
    if (loaded.some((other) => other.applicationId === cmd.applicationId)) {
      continue;
    }
    tracker.pending(cmd);
    removePromise.push(
      bot.helpers
        .deleteGuildApplicationCommand(cmd.id, cmd.guildId!)
        .then(() => {
          tracker.confirm(cmd, undefined);
        }),
    );
  }
  await Promise.all(removePromise);
}
