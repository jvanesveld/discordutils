import { RecursivePartial, TransformersDesiredProperties } from "../../deps.ts";
import { RequiredProps } from "../../mod.ts";
import { EventSource, Listener, OneShotSource } from "../utils/event-source.ts";
import {
  ConnectedArgs,
  InteractionArgs,
  MemberAddedArgs,
  MessageArgs,
  ReactionAddArgs,
  ReactionRemoveArgs,
  TypingStartArgs,
  UdpArgs,
  VoiceServerUpdateArgs,
  VoiceStateUpdateArgs,
} from "./args.ts";
import { createOnComponent } from "./filters/component.ts";
import {
  createOnApplication,
  createOnSubApplication,
  createOnSubgroupApplication,
} from "./filters/slash.ts";
import {
  createOnVoiceJoin,
  createOnVoiceLeave,
  createOnVoiceStateGuild,
} from "./filters/voice.ts";

export * from "./args.ts";

export function create<
  TProps extends
    & RecursivePartial<TransformersDesiredProperties>
    & RequiredProps,
>() {
  const sources = {
    message: new EventSource<MessageArgs<TProps>>(),
    ready: new OneShotSource<ConnectedArgs<TProps>>(),
    interaction: new EventSource<
      InteractionArgs<TProps>
    >(),
    reactionAdd: new EventSource<
      ReactionAddArgs<TProps>
    >(),
    reactionRemove: new EventSource<
      ReactionRemoveArgs<TProps>
    >(),
    typingStart: new EventSource<
      TypingStartArgs<TProps>
    >(),
    voiceStateUpdate: new EventSource<
      VoiceStateUpdateArgs<TProps>
    >(),
    voiceServerUpdate: new EventSource<VoiceServerUpdateArgs>(),
    memberAdded: new EventSource<
      MemberAddedArgs<TProps>
    >(),
    udp: new EventSource<UdpArgs>(),
  };
  const events = {
    onReady: sources.ready.addListener,
    onMessage: (
      listener: Listener<MessageArgs<TProps>>,
    ) =>
      sources.message.addListener((msg) => {
        if ("author" in msg && (msg.author as any).bot) {
          return;
        }
        listener(msg);
      }),
    onInteraction: sources.interaction.addListener,
    onReactionAdd: sources.reactionAdd.addListener,
    onReactionRemove: sources.reactionRemove.addListener,
    onTypingStart: sources.typingStart.addListener,
    onVoiceStateUpdate: sources.voiceStateUpdate.addListener,
    onVoiceServerUpdate: sources.voiceServerUpdate.addListener,
    onMemberAdded: sources.memberAdded.addListener,
    onUdp: sources.udp.addListener,
  };

  const filtered = {
    onComponent: createOnComponent<TProps>(events.onInteraction),
    onApplication: createOnApplication<TProps>(events.onInteraction),
    onSubApplication: createOnSubApplication<TProps>(events.onInteraction),
    onSubgroupApplication: createOnSubgroupApplication<TProps>(
      events.onInteraction,
    ),
    onVoiceStateGuild: createOnVoiceStateGuild<TProps>(
      events.onVoiceStateUpdate,
    ),
    onVoiceJoin: createOnVoiceJoin<TProps>(events.onVoiceStateUpdate),
    onVoiceLeave: createOnVoiceLeave<TProps>(events.onVoiceStateUpdate),
  };

  return { sources, events: { ...events, ...filtered } };
}
