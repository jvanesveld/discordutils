import {
  CompleteDesiredProperties,
  DiscordReady,
  Emoji,
  Interaction,
  Member,
  Message,
  RecursivePartial,
  SetupDesiredProps,
  TransformersDesiredProperties,
  TransformersObjects,
  User,
  VoiceState,
} from "../../deps.ts";
import { Behavior } from "../../mod.ts";

export type ShortP<Required = {}> =
  & RecursivePartial<TransformersDesiredProperties>
  & Required;

type Prop<
  Target extends TransformersObjects[keyof TransformersObjects],
  P extends ShortP,
> = SetupDesiredProps<
  Target,
  CompleteDesiredProperties<NoInfer<P>>,
  Behavior
>;

export type ConnectedArgs<P extends ShortP> = {
  payload: {
    shardId: number;
    v: number;
    user: Prop<User, P>;
    guilds: bigint[];
    sessionId: string;
    shard?: number[];
    applicationId: bigint;
  };
  rawPayload: DiscordReady;
};

export type MemberAddedArgs<P extends ShortP> = {
  member: Prop<Member, P>;
  user: Prop<User, P>;
};

export type MessageArgs<
  P extends ShortP,
> = Prop<
  Message,
  P
>;

export type TypingStartArgs<P extends ShortP> = {
  guildId: bigint | undefined;
  channelId: bigint;
  userId: bigint;
  timestamp: number;
  member: Prop<Member, P> | undefined;
};
export type VoiceStateUpdateArgs<P extends ShortP> = Prop<
  VoiceState,
  P
>;
export type VoiceServerUpdateArgs = {
  token: string;
  endpoint?: string;
  guildId: bigint;
};

export type InteractionArgs<
  P extends ShortP,
> = Prop<
  Interaction,
  P
>;

export type ReactionAddArgs<P extends ShortP> = {
  userId: bigint;
  channelId: bigint;
  messageId: bigint;
  guildId?: bigint;
  member?: Prop<Member, P>;
  user?: Prop<User, P>;
  emoji: Prop<Emoji, P>;
  messageAuthorId?: bigint;
  burst: boolean;
  burstColors?: string[];
};
export type ReactionRemoveArgs<P extends ShortP> = {
  userId: bigint;
  channelId: bigint;
  messageId: bigint;
  guildId?: bigint;
  emoji: Prop<Emoji, P>;
  burst: boolean;
};

export type UdpArgs = {
  guildId: bigint;
  data: Uint8Array;
};
