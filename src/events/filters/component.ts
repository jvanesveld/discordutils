import { Listener } from "../../utils/event-source.ts";
import { InteractionArgs, ShortP } from "../mod.ts";
import { EventSource } from "../../utils/event-source.ts";

export function createOnComponent<
  J extends ShortP & { interaction: { data: true } },
>(
  event: EventSource<
    InteractionArgs<J>
  >["addListener"],
) {
  return (
    customId: string,
    listener: Listener<InteractionArgs<J>>,
  ) =>
    event((data) => {
      if ((data as any).data?.customId !== customId) {
        return;
      }
      listener(data);
    });
}
