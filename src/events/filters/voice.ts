import { ShortP, VoiceStateUpdateArgs } from "../mod.ts";
import { asArray } from "../../utils/mod.ts";
import { Listener } from "../../utils/event-source.ts";
import { EventSource } from "../../utils/event-source.ts";

export function createOnVoiceStateGuild<
  J extends ShortP & { voiceState: { guildId: true } },
>(
  event: EventSource<
    VoiceStateUpdateArgs<J>
  >["addListener"],
) {
  return (
    guild: bigint | bigint[],
    listener: Listener<VoiceStateUpdateArgs<J>>,
  ) => {
    const guilds = asArray(guild);
    return event((voiceState) => {
      const check = (voiceState as any).guildId
        ? guilds.includes((voiceState as any).guildId)
        : false;
      if (!check) return;
      listener(voiceState);
    });
  };
}

export function createOnVoiceJoin<
  J extends ShortP & {
    voiceState: { guildId: true; channelId: true; userId: true };
  },
>(
  event: EventSource<
    VoiceStateUpdateArgs<J>
  >["addListener"],
) {
  return (
    guild: bigint | bigint[],
    listener: Listener<VoiceStateUpdateArgs<J>>,
  ) => {
    const connected = new Map<bigint, bigint>();
    return createOnVoiceStateGuild<J>(event)(guild, (voiceState) => {
      if ((voiceState as any).channelId) {
        const joined = connected.get((voiceState as any).userId) !==
          (voiceState as any).channelId;
        connected.set(
          (voiceState as any).userId,
          (voiceState as any).channelId,
        );
        if (!joined) {
          return;
        }
      } else {
        connected.delete((voiceState as any).userId);
        return;
      }
      listener(voiceState);
    });
  };
}

export function createOnVoiceLeave<
  J extends ShortP & {
    voiceState: { guildId: true; channelId: true; userId: true };
  },
>(
  event: EventSource<
    VoiceStateUpdateArgs<J>
  >["addListener"],
) {
  return (
    guild: bigint | bigint[],
    listener: Listener<VoiceStateUpdateArgs<J>>,
  ) => {
    return createOnVoiceStateGuild<J>(event)(guild, (voiceState) => {
      if ((voiceState as any).channelId == null) {
        listener(voiceState);
      }
    });
  };
}
