import { InteractionArgs } from "../args.ts";
import { ApplicationCommand, InteractionDataOption } from "../../../deps.ts";
import { asArray, Listener } from "../../utils/mod.ts";
import { ShortP } from "../mod.ts";
import { EventSource } from "../../utils/event-source.ts";

type ParseOptionType =
  | InteractionDataOption["value"]
  | Record<string, InteractionDataOption["value"]>;

type InterOptionsArgs<J extends ShortP & { interaction: { data: true } }> =
  & InteractionArgs<J>
  & { options: Record<any, any> };

function mapOptions(
  options: InteractionDataOption[],
): [string, ParseOptionType][] {
  const result = options.map((option) => [
    option.name,
    option.options
      ? Object.fromEntries(mapOptions(option.options))
      : option.value,
  ]);
  return result as [string, ParseOptionType][];
}

function addInput<J extends ShortP & { interaction: { data: true } }>(
  interaction: InteractionArgs<J>,
) {
  if ((interaction as any).data?.options == undefined) {
    return { options: {}, ...interaction };
  }
  const entries = mapOptions((interaction as any).data?.options);
  const options = Object.fromEntries(entries) as Record<
    string,
    ParseOptionType
  >;
  return { options, ...interaction };
}

export function createOnApplication<
  J extends ShortP & { interaction: { data: true } },
>(
  event: EventSource<
    InteractionArgs<J>
  >["addListener"],
) {
  return (
    cmd: ApplicationCommand | ApplicationCommand[],
    listener: Listener<InterOptionsArgs<J>>,
  ) => {
    const cmds = asArray(cmd).map((cmd) => cmd.id);
    return event((interaction) => {
      if (!cmds.includes((interaction as any).data?.id || 0n)) {
        return;
      }
      listener(addInput<J>(interaction));
    });
  };
}

export function createOnSubgroupApplication<
  J extends ShortP & { interaction: { data: true } },
>(
  event: EventSource<
    InteractionArgs<J>
  >["addListener"],
) {
  return (
    cmd: ApplicationCommand | ApplicationCommand[],
    group: string,
    listener: Listener<InterOptionsArgs<J>>,
  ) => {
    return createOnApplication<J>(event)(cmd, (data) => {
      if (typeof data.options[group] !== "object") {
        return;
      }
      listener({
        ...data,
        options: data.options[group] as Record<string, ParseOptionType>,
      });
    });
  };
}

export function createOnSubApplication<
  J extends ShortP & { interaction: { data: true } },
>(
  event: EventSource<
    InteractionArgs<J>
  >["addListener"],
) {
  return (
    options: {
      cmd: ApplicationCommand | ApplicationCommand[];
      sub: string;
      group?: string;
    },
    listener: Listener<InterOptionsArgs<J>>,
  ) => {
    const filtered = (data: InterOptionsArgs<J>) => {
      const check = options.sub in data.options &&
        (data.options[options.sub] === undefined ||
          typeof data.options[options.sub] == "object");
      if (!check) {
        return;
      }
      listener({
        ...data,
        options: data.options[options.sub] as Record<string, ParseOptionType>,
      });
    };

    if (options.group) {
      createOnSubgroupApplication<J>(event)(
        options.cmd,
        options.group,
        filtered,
      );
    } else {
      createOnApplication<J>(event)(options.cmd, filtered);
    }
  };
}
