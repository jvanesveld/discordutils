import { loadLocalOrYoutube } from "../deps.ts";
import { loadSpotify, matchSpotify } from "./spotify/load-source.ts";

export async function audioQuery(query: string) {
  const spotifyMatch = matchSpotify(query);
  if (spotifyMatch !== undefined) {
    return await loadSpotify(spotifyMatch);
  }

  return loadLocalOrYoutube(query);
}
