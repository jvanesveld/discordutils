import { audioQuery } from "./src/audio-query.ts";
import {
  createBot,
  CreateBotOptions,
  DesiredPropertiesBehavior,
  enableAudioPlugin,
  Intents,
  RecursivePartial,
  TransformersDesiredProperties,
} from "./deps.ts";
import { create } from "./src/events/mod.ts";
import { addCoreFunctions } from "./src/mod.ts";

export type { AudioSource, Intents, QueuePlayer } from "./deps.ts";
export { createAudioSource, encodePCMAudio } from "./deps.ts";

export type Behavior = DesiredPropertiesBehavior.RemoveKey;

export type RequiredProps = {
  interaction: { data: true };
  voiceState: { guildId: true; userId: true; channelId: true; sessionId: true };
  user: { toggles: true };
  message: { author: true };
};

export async function initBot<
  TProps extends
    & RecursivePartial<TransformersDesiredProperties>
    & RequiredProps,
>(
  token: string,
  intents = Intents.Guilds |
    Intents.GuildMembers |
    Intents.GuildMessages |
    Intents.GuildMessageReactions |
    Intents.GuildVoiceStates |
    Intents.GuildMessageTyping,
  desiredProperties: TProps,
) {
  const { sources, events } = create<TProps>();
  const createOptions: CreateBotOptions<TProps, Behavior> = {
    token: token,
    intents,
    desiredProperties,
    events: {
      ready: (payload, rawPayload) =>
        sources.ready.trigger({ payload, rawPayload }),
      messageCreate: (msg) => {
        sources.message.trigger(msg);
      },
      interactionCreate: (interaction) =>
        sources.interaction.trigger(interaction),
      reactionAdd: (payload) => sources.reactionAdd.trigger(payload),
      reactionRemove: (payload) => sources.reactionRemove.trigger(payload),
      typingStart: (payload) => sources.typingStart.trigger(payload),
      voiceStateUpdate: (voiceState) => {
        sources.voiceStateUpdate.trigger(voiceState);
      },
      voiceServerUpdate: (payload) => {
        sources.voiceServerUpdate.trigger(payload);
      },
      guildMemberAdd: (member, user) =>
        sources.memberAdded.trigger({ member, user }),
    },
  };
  const baseBot = await createBot<TProps, Behavior>(createOptions);

  const audioBot = enableAudioPlugin<TProps, Behavior>(baseBot, audioQuery);
  await audioBot.start();

  const ready = new Promise<void>((resolve) => {
    events.onReady(() => {
      resolve();
    });
  });
  await ready;
  return addCoreFunctions(audioBot, events);
}

export { setSpotifyKeys } from "./src/spotify/authorize.ts";
